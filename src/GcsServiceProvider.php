<?php

namespace Drupal\gcs;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

class GcsServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('file_system');
    $definition->setClass('Drupal\gcs\GcsFileSystem\GcsFileSystem');
  }
}
