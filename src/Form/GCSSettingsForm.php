<?php

namespace Drupal\gcs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class GCSSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gcs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('gcs.settings');

    $form['instructions'] = [
      '#type' => 'item',
      '#markup' => $this->t('<p><strong>Instructions</strong><br/>Ensure that the bucket you\'re using is viewable to the public; member name "allUsers" and Role "Storage Object Viewer" work pretty well.</p>'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Google Cloud Storage Enabled'),
      '#description' => $this->t('Keep this unchecked until you have all permissions set, and your initial copy completed.'),
      '#default_value' => $config->get('gcs.status'),
    ];

    $form['project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#description' => $this->t('Your Google Cloud Project ID.'),
      '#default_value' => $config->get('gcs.project_id'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['bucket_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bucket Name'),
      '#description' => $this->t('Your bucket name. Keep it simple. This is just the bucket name, no protocol information, etc.'),
      '#default_value' => $config->get('gcs.bucket_name'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('gcs.settings');
    $config->set('gcs.project_id', $form_state->getValue('project_id'));
    $config->set('gcs.bucket_name', $form_state->getValue('bucket_name'));
    $config->set('gcs.status', $form_state->getValue('status'));

    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gcs.settings',
    ];
  }
}
